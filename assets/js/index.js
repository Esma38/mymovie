//import $ from 'jquery';
 window.jQuery = $;
 window.$ = $;
 //import 'bootstrap';
// import './style.css';
 //import './style.scss'

//document.getElementById('navbar').style.color = 'red';


$(document).ready(() => {
    $('#searchForm').on('submit', (e) => {
        let searchText = $('#searchText').val();
        getMovies(searchText);
        e.preventDefault();
    });
});

 function getMovies(searchText) {
     const key = "e4906b86ac736d9fb8a69209493f94d2";
     axios.get(`https://api.themoviedb.org/3/search/movie?api_key=${key}&query=${searchText}`)
         .then((response) => {
             console.log(response);
             let movies = response.data.results;
             let output = '';
             for (const movie of response.data.results)
              $.each(movies, (index, element) => {
                  output += `
                  <div class="col-md-3">
                  <div class="well text-center">
                  <img src="https://image.tmdb.org/t/p/w500/${element.poster_path}" class="img-fluid">
                  <h5>${element.title}</h5>
                  <a onclick="movieSelected('${element.data}')" class="btn btn-warning text-center" href="#">Movie Details</a>
                  </div>
                  </div>
                  `;
              });

             $('#movies').append(output);
         })
         .catch((err) => {
             console.log(err);
         });
 }


   function movieSelected(id) {
    sessionStorage.setItem('movieId', id);
    window.location = "movie.html";
    return false;
}
/*
function getMovie() {
    const key = "e4906b86ac736d9fb8a69209493f94d2";
    let movieId = sessionStorage.getItem('movieId');

    axios.get('https://api.themoviedb.org/3/search='+movieId)
        .then((response) => {
            console.log(response);
            })
        .catch((err) => {
            console.log(err);
        });
}

*/